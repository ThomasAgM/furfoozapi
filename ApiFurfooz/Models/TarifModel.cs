﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Models
{
    public class TarifModel
    {
        public int Age { get; set; }
        public double Price { get; set; }
        public bool IsDeleted { get; set; }
    }
}