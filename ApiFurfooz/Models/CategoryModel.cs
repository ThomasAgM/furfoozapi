﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string Name_fr { get; set; }
        public string Name_nl { get; set; }
        public string Name_en { get; set; }
        public bool IsDeleted { get; set; }

    }
}