﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Models
{
    public class BuvetteModel
    {
        public int Id { get; set; }
        public string Name_fr { get; set; }
        public string Name_en { get; set; }
        public string Name_nl { get; set; }
        public double Price { get; set; }
        public bool IsDeleted { get; set; }
        public int CategoryProductId { get; set; }

    }
}