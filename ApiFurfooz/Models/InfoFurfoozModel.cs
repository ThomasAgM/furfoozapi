﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace ApiFurfooz.Models
{
    public class InfoFurfoozModel
    {
        public MailAddress Email { get; set; }
        public string Tel { get; set; }
        public string NomConservateur { get; set; }
        public bool IsDeleted { get; set; }
    }
}