﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;

namespace ApiFurfooz.Controllers
{
    public class BuvetteController : ApiController
    {
        [HttpGet]
        public IEnumerable<BuvetteModel> Get()
        {
            IEnumerable<BuvetteModel> Buvettes = ServiceLocator.GetService<BuvetteService>().GetAll();
            return Buvettes;
        }

        [HttpGet]

        public Buvette GetById(int id)
        {
            return ServiceLocator.GetService<BuvetteService>().GetById(id);
        }

        public void Update (Buvette a)
        {
            ServiceLocator.GetService<BuvetteService>().Update(a);
        }

        public void Delete (int id)
        {
            ServiceLocator.GetService<BuvetteService>().Delete(id);
        }

        public void Insert (Buvette a)
        {
            ServiceLocator.GetService<BuvetteService>().Insert(a);
        }

    }
}