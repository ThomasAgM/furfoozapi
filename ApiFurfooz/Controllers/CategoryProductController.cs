﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.Models;
using ApiFurfooz.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace ApiFurfooz.Controllers
{
    public class CategoryProductController
    {
        [HttpGet]
        public IEnumerable<CategoryProductModel> Get()
        {
            IEnumerable<CategoryProductModel> Categories = ServiceLocator.GetService<CategoryProductService>().GetAll();
            return Categories;
        }

        [HttpGet]

        public CategoryProduct GetById(int id)
        {
            return ServiceLocator.GetService<CategoryProductService>().GetById(id);
        }

        public void Update(CategoryProduct a)
        {
            ServiceLocator.GetService<CategoryProductService>().Update(a);
        }

        public void Delete(int id)
        {
            ServiceLocator.GetService<CategoryProductService>().Delete(id);
        }

        public void Insert(CategoryProduct a)
        {
            ServiceLocator.GetService<CategoryProductService>().Insert(a);
        }

    }
}