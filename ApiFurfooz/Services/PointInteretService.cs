﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class PointInteretService
    {
        private SqlConnection _connection;
        private string connectionString;
        public PointInteretService()
        {
            connectionString = ConfigurationManager.ConnectionStrings["maConnection"].ConnectionString;
            _connection = new SqlConnection(connectionString);
        }

        public IEnumerable<PointInteret> GetAll()
        {
            IEnumerable<PointInteret> pis = ServiceLocator.GetService<PointInteretRepository>().Get();
            return pis;
        }

        public PointInteret GetById(int id)
        {
            PointInteret pi = ServiceLocator.GetService<PointInteretRepository>().GetById(id);
            return pi;
        }

        public void Insert(PointInteret a)
        {
            ServiceLocator.GetService<PointInteretRepository>().Insert(a);
        }

        public void Update(PointInteret a)
        {
            ServiceLocator.GetService<PointInteretRepository>().Update(a);
        }

        public void Delete(int id)
        {
            ServiceLocator.GetService<PointInteretRepository>().Delete(id);
        }
    }
}