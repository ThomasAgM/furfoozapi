﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class CategoryService
    {
        private SqlConnection _connection;
        private string connectionString;
        public CategoryService()
        {
            connectionString = ConfigurationManager.ConnectionStrings["maConnection"].ConnectionString;
            _connection = new SqlConnection(connectionString);
        }

        public IEnumerable<Category> GetAll()
        {
            IEnumerable<Category> cats = ServiceLocator.GetService<CategoryRepository>().Get();
            return cats;
        }

        public Category GetById(int id)
        {
            Category cat = ServiceLocator.GetService<CategoryRepository>().GetById(id);
            return cat;
        }

        public void Insert(Category a)
        {
            ServiceLocator.GetService<CategoryRepository>().Insert(a);
        }

        public void Update(Category a)
        {
            ServiceLocator.GetService<CategoryRepository>().Update(a);
        }

        public void Delete(int id)
        {
            ServiceLocator.GetService<CategoryRepository>().Delete(id);
        }

    }
}