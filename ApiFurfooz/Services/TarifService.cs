﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class TarifService
    {
        private SqlConnection _connection;
        private string connectionString;
        public TarifService()
        {
            connectionString = ConfigurationManager.ConnectionStrings["maConnection"].ConnectionString;
            _connection = new SqlConnection(connectionString);
        }

        public IEnumerable<Tarif> GetAll()
        {
            IEnumerable<Tarif> tarifs = ServiceLocator.GetService<TarifRepository>().Get();
            return tarifs;
        }

        public Tarif GetById(int id)
        {
            Tarif tarif = ServiceLocator.GetService<TarifRepository>().GetById(id);
            return tarif;
        }

        public void Insert(Tarif a)
        {
            ServiceLocator.GetService<TarifRepository>().Insert(a);
        }

        public void Update(Tarif a)
        {
            ServiceLocator.GetService<TarifRepository>().Update(a);
        }

        public void Delete(int id)
        {
            ServiceLocator.GetService<TarifRepository>().Delete(id);
        }
    }
}