﻿using ApiFurfooz.DAL.Entities;
using ApiFurfooz.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class InfoFurfoozService
    {
        private SqlConnection _connection;
        private string connectionString;
        public InfoFurfoozService()
        {
            connectionString = ConfigurationManager.ConnectionStrings["maConnection"].ConnectionString;
            _connection = new SqlConnection(connectionString);
        }

        public IEnumerable<InfoFurfooz> GetAll()
        {
            IEnumerable<InfoFurfooz> infos = ServiceLocator.GetService<InfoFurfoozRepository>().Get();
            return infos;
        }

        public InfoFurfooz GetById(int id)
        {
            InfoFurfooz info = ServiceLocator.GetService<InfoFurfoozRepository>().GetById(id);
            return info;
        }

        public void Insert(InfoFurfooz a)
        {
            ServiceLocator.GetService<InfoFurfoozRepository>().Insert(a);
        }

        public void Update(InfoFurfooz a)
        {
            ServiceLocator.GetService<InfoFurfoozRepository>().Update(a);
        }

        public void Delete(int id)
        {
            ServiceLocator.GetService<InfoFurfoozRepository>().Delete(id);
        }
    }
}