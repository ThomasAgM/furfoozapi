﻿using ApiFurfooz.DAL.Repositories;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ApiFurfooz.Services
{
    public class ServiceLocator
    {
        private static ServiceCollection services = new ServiceCollection();
        private static ServiceProvider Provider;

        static ServiceLocator()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            string provider =
                ConfigurationManager.ConnectionStrings["default"].ProviderName;
            services.AddTransient<BuvetteRepository>(
                (x) => new BuvetteRepository(connectionString, provider)
                );
            services.AddTransient<CategoryProductRepository>(
                (x) => new CategoryProductRepository(connectionString, provider)
                );
            services.AddTransient<CategoryRepository>(
                (x) => new CategoryRepository(connectionString, provider)
                 );
            services.AddTransient<InfoFurfoozRepository>(
                (x) => new InfoFurfoozRepository(connectionString, provider)
                 );
            services.AddTransient<PointInteretRepository>(
                (x) => new PointInteretRepository(connectionString, provider)
                 );
            services.AddTransient<TarifRepository>(
                (x) => new TarifRepository(connectionString, provider)
                 );

            services.AddTransient<BuvetteService>(
                (x) => new BuvetteService()
                );
            services.AddTransient<CategoryProductService>(
                (x) => new CategoryProductService()
                );
            services.AddTransient<CategoryService>(
                (x) => new CategoryService()
                );
            services.AddTransient<InfoFurfoozService>(
                (x) => new InfoFurfoozService()
                );
            services.AddTransient<PointInteretService>(
                (x) => new PointInteretService()
                );
            services.AddTransient<TarifService>(
                (x) => new TarifService()
                );

            Provider = services.BuildServiceProvider();
        }
        public static T GetService<T>()
        {
            return Provider.GetService<T>();
        }
    }
}